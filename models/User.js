const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = new Schema({
  fullname: {
    type: String
  },
  username: {
    type: String,
    required: [true, "Username must be filled"],
    unique: [true, "Username already exist. Use another username"],
    minlength: [3, "too short, min 3 character"],
    validate: function(username) {
      return /^\S*$/.test(username);
    }
  },
  email: {
    type: String,
    required: [true, "Email must be filled"],
    unique: [true, "Email already registered"],
    validate: function validateEmail(email) {
      var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(String(email).toLowerCase());
    }
  },
  password: {
    type: String,
    required: [true, "Password must be filled"],
    minlength: [5, "too short, min 5 character"]
  },
  role: {
    type: String,
    enum: ["generator", "contributor"],
    default: "contributor"
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
});

const User = mongoose.model("User", userSchema);
userSchema.plugin(uniqueValidator);

module.exports = User;
