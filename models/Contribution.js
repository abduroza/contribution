const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const contributionSchema = new Schema({
  uniqueCode: {
    type: String,
    required: [true, "uniqueCode must be filled"]
  },
  link: {
    type: String,
    required: [true, "link must be filled"]
  },
  expiredLinkDate: {
    type: Date
  },
  count: {
    type: Number,
    default: 0
  },
  generatorId: {
    type: Schema.Types.ObjectId,
    ref: "User"
  },
  listContributorId: [
    {
      type: Schema.Types.ObjectId,
      ref: "User"
    }
  ]
});

const Contributor = mongoose.model("Contributor", contributionSchema);

module.exports = Contributor;
