const User = require("../models/User");
const Contribution = require("../models/Contribution");
const { sucRes, failRes, randomLink } = require("../helpers/resFormat");

async function getContributor(req, res) {
  let contribution = await Contribution.find({ generatorId: req.decoded._id })
    .select("id link expiredLinkDate count listContributorId generatorId")
    .populate({
      path: "generatorId",
      select: ["id", "fullname", "username", "email", "role"]
    });
  return res.status(200).json(sucRes(contribution, "Your contribution"));
}

async function addNewLink(req, res) {
  let contribution = await Contribution.findOne({
    generatorId: req.decoded._id,
    expiredLinkDate: { $gt: Date.now() }
  });
  if (contribution != null) {
    return res
      .status(400)
      .json(failRes("Can't create new link. Your last link still active"));
  }

  let uniqueCode = randomLink();
  let uniqueLink =
    "http://" + req.get("host") + "/api/v1/user/register/" + uniqueCode;
  let expiredLinkDate = new Date(new Date().setDate(new Date().getDate() + 7));

  let linkContribution = await Contribution.create({
    uniqueCode: uniqueCode,
    link: uniqueLink,
    expiredLinkDate: expiredLinkDate
  });

  linkContribution.generatorId = req.decoded._id;
  linkContribution.save();

  return res.status(201).json(sucRes(linkContribution, "Success add new link"));
}

module.exports = { getContributor, addNewLink };
