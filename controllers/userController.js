const User = require("../models/User");
const Contribution = require("../models/Contribution");
const { sucRes, failRes, randomLink } = require("../helpers/resFormat");
const saltRounds = 10;
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

async function registerGenerator(req, res) {
  try {
    if (req.body.role == null || req.body.role == "contributor") {
      return res
        .status(403)
        .json(
          failRes("You must be get unique link to register as contributor")
        );
    }
    let hash = await bcrypt.hash(req.body.password, saltRounds);

    let user = await User.create({
      fullname: req.body.fullname,
      username: req.body.username,
      email: req.body.email,
      password: hash,
      role: req.body.role
    });

    let token = await jwt.sign(
      {
        _id: user._id,
        username: user.username,
        email: user.email,
        role: user.role
      },
      process.env.TOKEN_SECRET
    );

    let uniqueCode = randomLink();
    let uniqueLink =
      "http://" + req.get("host") + "/api/v1/user/register/" + uniqueCode;
    let expiredLinkDate = new Date(
      new Date().setDate(new Date().getDate() + 7)
    );

    let linkContribution = await Contribution.create({
      uniqueCode: uniqueCode,
      link: uniqueLink,
      expiredLinkDate: expiredLinkDate
    });

    linkContribution.generatorId = user._id;
    linkContribution.save();

    let result = {
      _id: user._id,
      fullname: user.fullname,
      username: user.username,
      email: user.email,
      role: user.role,
      token: token,
      link: uniqueLink
    };

    return res
      .status(201)
      .json(sucRes(result, "Success add new user generator"));
  } catch (err) {
    return res.status(400).json(failRes(err.message, "Error"));
  }
}

async function registerContributor(req, res) {
  try {
    let findContributor = await Contribution.findOne({
      uniqueCode: req.params.unique
    });

    if (findContributor == null) {
      return res.status(404).json(failRes("Link not valid"));
    } else if (Date.now() >= findContributor.expiredLinkDate) {
      return res.status(400).json(failRes("Link has expired"));
    }

    let hash = await bcrypt.hash(req.body.password, saltRounds);

    let user = await User.create({
      fullname: req.body.fullname,
      username: req.body.username,
      email: req.body.email,
      password: hash
    });

    let token = await jwt.sign(
      {
        _id: user._id,
        username: user.username,
        email: user.email,
        role: user.role
      },
      process.env.TOKEN_SECRET
    );

    let contribution = await Contribution.findOneAndUpdate(
      { uniqueCode: req.params.unique },
      { count: findContributor.count + 1 }
    );
    contribution.listContributorId.push(user._id);
    contribution.save();

    let result = {
      _id: user._id,
      fullname: user.fullname,
      username: user.username,
      email: user.email,
      role: user.role,
      token: token
    };
    return res
      .status(201)
      .json(sucRes(result, "Success add new user contributor"));
  } catch (err) {
    return res.status(400).json(failRes(err.message, "Error"));
  }
}

async function login(req, res) {
  let findUser = await User.findOne({
    $or: [{ username: req.body.username }, { email: req.body.email }]
  });
  if (!findUser)
    return res.status(404).json(failRes("Username or Email Not Found"));

  let password = await bcrypt.compare(req.body.password, findUser.password);
  if (!password) return res.status(401).json(failRes("Invalid Password"));

  let token = await jwt.sign(
    {
      _id: findUser._id,
      username: findUser.username,
      email: findUser.email,
      role: findUser.role
    },
    process.env.TOKEN_SECRET
  );
  let dataLogin = {
    _id: findUser._id,
    role: findUser.role,
    username: findUser.username,
    email: findUser.email,
    token: token
  };
  res.status(200).json(sucRes(dataLogin, "Login success"));
}

module.exports = { registerGenerator, registerContributor, login };
