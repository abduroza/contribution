const sucRes = function(result, message) {
  return {
    success: true,
    messages: message,
    results: result
  };
};
const failRes = function(result, message) {
  return {
    success: false,
    messages: message,
    results: result
  };
};
const randomLink = function() {
  let chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  let link = "";
  for (let i = 16; i > 0; --i) {
    link += chars[Math.round(Math.random() * (chars.length - 1))];
  }
  return link;
};

module.exports = { sucRes, failRes, randomLink };
