var router = require("express").Router();
var contributorController = require("../../controllers/contributorController");
var auth = require("../../middleware/auth");

router.get("/", auth, contributorController.getContributor);
router.post("/addlink", auth, contributorController.addNewLink);

module.exports = router;
