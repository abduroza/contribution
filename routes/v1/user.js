var router = require("express").Router();
var userController = require("../../controllers/userController");

router.post("/register", userController.registerGenerator);
router.post("/register/:unique", userController.registerContributor);
router.post("/login", userController.login);

module.exports = router;
