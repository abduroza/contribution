var router = require("express").Router();
var userRouter = require("./v1/user");
var contributionRouter = require("./v1/contribution");

router.use("/user", userRouter);
router.use("/contribution", contributionRouter);

module.exports = router;
