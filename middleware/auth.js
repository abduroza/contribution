const jwt = require("jsonwebtoken");
const { failRes } = require("../helpers/resFormat");
const User = require("../models/User");

async function auth(req, res, next) {
  let bearerToken = await req.headers.authorization;
  if (!bearerToken) return res.status(401).json(failRes("Token Not Available"));
  let splitToken = bearerToken.split(" ");
  try {
    let decoded = await jwt.verify(splitToken[1], process.env.TOKEN_SECRET);
    req.decoded = decoded;
    let user = await User.findById(req.decoded._id);
    if (!user)
      return res.status(410).json(failRes("User Gone Due to Already Deleted"));
    next();
  } catch (err) {
    return res.status(401).json(failRes("Invalid Token"));
  }
}

module.exports = auth;
